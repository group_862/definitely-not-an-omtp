function [DMP] = LearnJDMP(path,dt,N) % Simiilar to Learn QDMP, but this time we only need joint positions)
    %set DMP parameters
    if nargin == 3
        DMP.N = N;
    else
        DMP.N = 20;  %Need to double check why 20
    end
    DMP.dt=dt;DMP.a_z=48;DMP.a_x=2;
    %learning joint positions
    DMP=DMP_rlearn(path,DMP);
end

