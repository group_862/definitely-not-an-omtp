%%
clc
% clear all
%mj_close;
close all
dt=0.01; %100 hz - meaning a delta time of 0.01 sec 
path('..\apimex',path)
path('..\Mujoco_lib',path)
load test_trj.mat

%% TODO: Implement the encoding step for the Joint DMP from the previous
%exercise. 
JointDMP = LearnJDPM(qJoints, dt);

%Goal 
Tf = (length(Qpath)+1)*DMP.dt;
Xmin = exp(-DMP.a_x*Tf/DMP.tau);


%init. states for position dmp
S_joint.y = JointDMP.y0;
S_joint.z = zeros(1,7);
S_joint.x = 1;

%% Execute in simulation

try
  
    mj_connect;
    
    Startjoints = qJoints(1,:);
    
    %% move to init position
    JmoveM(Startjoints,1);
    Con = mj_get_control;
    i=1;
    tn=0;
    st = tic;
     
    while S_joint.x > Xmin 
        [S_joint]=DMP_integrate(JointDMP,S_joint,0);
        jN(i,:) = S_joint.y;
        jjN(i,:) = S_joint.z;
        
        %% Send to robot
        Con.ctrl(1:7) = S_joint.y
        Con.ctrl(8:14)=S_joint.z;
        mj_set_control(Con);
       
        
        %% sinhronisation 
        tn = tn+dt;
        if tn>toc(st)
            pause(tn-toc(st))
        end
        i=i+1;
    end
       %toc
       
    %% move to start position    
    JmoveM(Startjoints,1);
    
    %close sim connection
    mj_close;
    
catch ME
    rethrow(ME);
end
