%%
clc
% clear all
close all
dt = 0.01; %dt = time interval, as the data is recorded with 100 hz, the time is 0.01 sec
load test_trj.mat

%encode into DMP
[DMP,QDMP] = LearnQDMP(Qpath,dt);

% TODO: for joint trajectories define a function for calculating a Joint
% DMP parameters Inputs: qJoints and dt Output: JDMP parameters. the
% function should be simmilar as LearnQDMP (without the orientation part)
JointDMP = LearnJDPM(qJoints,dt);


%% test different parameters for example: %Tau is from the temporal scaling 
%DMP.tau=5;
%QDMP.tau=5;
%JDMP.tau=5;

%%Calculate final phase for the DMP integration
Tf = (length(Qpath)+1)*DMP.dt;

Xmin = exp(-DMP.a_x*Tf/DMP.tau);

%init. states for position dmp
S_position.y = DMP.y0;
S_position.z = zeros(1,7);
S_position.x = 1;

%init. states for position dmp
S_joint.y = JointDMP.y0;
S_joint.z = zeros(1,7);
S_joint.x = 1;

%init. states for quaternion dmp
S_quaterion.q = QDMP.q0;
S_quaterion.o = zeros(3,1);
S_quaterion.x = 1;


%%
i=1;
while S_position.x > Xmin
    % position DMP
    [S_position] = DMP_integrate(DMP,S_position,0);
    xN(i,:) = S_position.y;

    %% TODO: Joint DMP integrate Configure the a simillar function as for the position part of the DMP
    [S_joint] = DMP_integrate(JointDMP,S_joint,0);
    jN(i,:) = S_joint.y;
    %%
    % Quaternion DMP
    [S_quaterion] = qDMP_integrate(QDMP,S_quaterion,0);
    qQ(i,:) = S_quaterion.q; % just for plotting

    i = i+1;
end

%% Plot Position part of the trajectory
plot(xN,'r')  % plot DMP trajectory
hold on
plot(Qpath(:,1:3),'--b') % plot example trajectory

%% Plot Orientation part of the trajectory
figure(2)

%plot(qQ,'r') % plot QDMP trajectory - having issues polt the 520x1
%quaterion data
hold on
plot(Qpath(:,4:7),'--b') % plot example trajectory

% Plot Joint trajectories
figure(3)
plot(jN,'r') % plot QDMP trajectory
hold on
plot(qJoints,'--b') % plot example trajectory
