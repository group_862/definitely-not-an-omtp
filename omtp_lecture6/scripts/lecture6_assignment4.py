#!/usr/bin/env python

import sys
import copy
from numpy import pi
import rospy
import moveit_commander
import moveit_msgs.msg
import actionlib
import geometry_msgs
import tf2_ros
import tf2_geometry_msgs
from tf.transformations import quaternion_from_euler

from omtp_gazebo.srv import VacuumGripperControl
from omtp_gazebo.msg import LogicalCameraImage
from std_srvs.srv import Empty


class Robot2Controller(object):

    def __init__(self):
        """Initialisation of ROS Node, TF, and MoveIt Commander tools.
        """
        rospy.init_node('lecture6_assignment4', anonymous=True)  # Initialize ROS node to transform object pose.
        rospy.loginfo('Initializing lecture6_assignment4 ROS Node.')

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.loginfo('Initializing MoveIt Commander')
        self.robot2_group = moveit_commander.MoveGroupCommander("robot2")  # MoveGroup Commander Object for robot2.
        self.robot2_client = actionlib.SimpleActionClient('execute_trajectory',
                                                          moveit_msgs.msg.ExecuteTrajectoryAction)
        self.robot2_client.wait_for_server()
        rospy.loginfo('Execute Trajectory server is available for robot2')

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

    def PickAndPlace(self):
        """ Pick and place sequence for Robot2 in OMTP Course, lecture 6, exercise 4.
        """
        # Step 0: Spawn an object
        self.spawn_object_rosservice()
        # Step 1: Move robot to upwards position (starting position)
        self.move_robot2_to_named_pose('R2Up')

        # Step 1: Move to pre-grasp position
        self.move_robot2_to_named_pose('R2PreGrasp')

        # Step 3: Turn on end-effector suction gripper
        self.gripper2_enable_rosservice(True)

        # Step 4: Get message from logical camera (all objects)
        camera_msg = rospy.wait_for_message('omtp/logical_camera_2', LogicalCameraImage)

        # Step 5: Check camera message for existance of "object" and get it
        object_world_pose = self.get_object_pose_logical_camera2(camera_msg)
        pose = object_world_pose.pose

        # Step 6: Move robot (end-effector tip) to object location
        rotation_transform = quaternion_from_euler(pi, 0, 0)  # object's Z-axis points opposite direction of EE Z-axis. Rotate z-axis by 180deg.
        pose.orientation.x = rotation_transform[0]
        pose.orientation.y = rotation_transform[1]
        pose.orientation.z = rotation_transform[2]
        pose.orientation.w = rotation_transform[3]
        pose.position.z += pose.position.z  # Coordinate frame is located in the middle of the object. We want top of the object. So double distance.
        self.move_robot2_to_pose(pose)

        # Step 7: Lift robot with object slightly up to avoid collisions.
        pose.position.z += 0.3
        self.move_robot2_to_pose(pose)

        # Step 8: Move to bin location.
        self.move_robot2_to_named_pose('R2Place')

        # Step 9: Turn off suction end-effector
        self.gripper2_enable_rosservice(False)

    def get_object_pose_logical_camera2(self, data):
        """Function that takes logical camera message, looks for "object" presence and returns it.

        Arguments:
            data {msg.LogicalCameraImage} -- msg from logical camera topic

        Returns:
            msg.PoseStamped -- Full message of 'object' (time, frame_id, pose)
        """
        # Check if the logical camera has seen our box which has the name 'object'.
        if (data.models[-1].type == 'object'):
            object_pose = geometry_msgs.msg.PoseStamped()  # Create a pose stamped message type from the camera image topic.
            object_pose.header.stamp = rospy.Time.now()
            object_pose.header.frame_id = 'logical_camera_2_frame'
            object_pose.pose = data.models[-1].pose
            while True:
                try:
                    object_world_pose = self.tf_buffer.transform(object_pose, 'world')  # Transform object frame from camera frame to world
                    return object_world_pose
                except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                    continue
        else:
            rospy.loginfo('No object present near Robot 2')

    def move_robot2_to_named_pose(self, pose_name):
        """Move Robot2 to a defined position (e.g. "R2Home") via MoveIt Commander.

        Arguments:
            pose_name {string} -- Name of defined Robot2 position. Available: 'R2Home', 'R2Up', 'R2PreGrasp', 'R2Place'.
        """
        self.robot2_group.set_named_target(pose_name)
        robot2_plan_pose = self.robot2_group.plan()  # Plan to the desired joint-space goal using the default planner (RRTConnect).
        robot2_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()  # Create a goal message object for the action server.
        robot2_goal.trajectory = robot2_plan_pose  # Update the trajectory in the goal message.
        self.robot2_client.send_goal(robot2_goal)  # Send the goal to the action server.
        self.robot2_client.wait_for_result()
        # NOTE: last 5 lines can be replaced with 'robot2_group.go()'. See example in move_robot2_to_pose()

    def move_robot2_to_pose(self, pose):
        """Move Robot2 to a given position (e.g. "R2Home") via MoveIt Commander.

        Arguments:
            pose {geometry_msgs.msg._Pose.Pose} -- ROS message with x,y,z position (in meters) and x,y,z,w orientation (in quaternions)
        """
        self.robot2_group.set_pose_target(pose)
        self.robot2_group.go()

    def gripper2_enable_rosservice(self, command):
        """ROS Service call to /gripper2/control to enable/disable suction gripper

        Arguments:
            command {Bool} -- True to enable gripper, False to disable
        """
        # Name found by running Gazebo world and running 'rosservice list'
        service = rospy.ServiceProxy('/gripper2/control', VacuumGripperControl)
        service(command)
        rospy.loginfo('Robot2 suction gripper status: %s', command)

    def spawn_object_rosservice(self):
        """ROS Service call to /spawn_object_at_robot2 to spawn an object
        """
        service = rospy.ServiceProxy('/spawn_object_at_robot2', Empty)
        service()
        rospy.loginfo('Spawned an object near Robot 2.')


if __name__ == '__main__':
    RC = Robot2Controller()
    RC.PickAndPlace()
