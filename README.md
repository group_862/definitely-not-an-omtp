# Introduction

This is the project of Group 862 following the "Object Manipulation Task Planning" course in Robotics 2nd Semester, Master's Degree, Aalborg University in Denmark.

**Getting Started** describes how to run the code on your machine and what prerequisites you first must get.

**Project description** provides a detailed documentation of the work.

- Lecture 1 - Building a Robot Simulation Environment in ROS
- Lecture 3/4 - Dynamic Movement Primitives in Matlab 
- Lecture 5 - Manipulation with MoveIt
- Lecture 6 - Object detection and grasping
- Lecture 7 - Behavior Design with State Machines
- Lecture 8 - CNNs in Practical Robotic Applications
- Lecture 9 - Deep Reinforcement Learning for Robot Control


**Authors**

- **Jevgenijs Galaktionovs** - *Group 862 member* - [eugenegalaxy](https://github.com/eugenegalaxy)
- **Jesper Bro** - *Group 862 member* - [jesperBKR](https://github.com/jesperBKR)
- **Karl Hugo Bartolomeus Markoff** - *Group 862 member* - [HugoMarkoff](https://github.com/HugoMarkoff)
- **Rebecca Nekou Malihi** - *Group 862 member* - [rebeccamalihi](https://github.com/rebeccamalihi)
---
# Getting Started

This section will provide all necessary information to successfully run the code on your computer. In case of any issues, email one of the Authors, listed at the end of this README.

### Prerequisites

* Ubuntu 18.04 (Bionic Beaver)

### Installation

ROS Melodic:

        sudo apt install ros-melodic-desktop-full
Joint State Publisher GUI (ROS package):

        sudo apt install ros-melodic-joint-state-publisher-gui
Gazebo 9.13 (last version of Gazebo 9.x):

- Follow instructions on http://gazebosim.org/tutorials?cat=install&tut=install_ubuntu&ver=9.0

Gazebo ROS Control:

        sudo apt install ros-melodic-gazebo-ros-control
Gazebo Messages:

        sudo apt install ros-melodic-gazebo-msgs

Gazebo Plugins:

        sudo apt install ros-melodic-gazebo-plugins
ROS Controllers:

        sudo apt install ros-melodic-ros-controllers
ROS MoveIt:

        sudo apt install ros-melodic-moveit
ROS MoveIt Setup Assistant:

        sudo apt install ros-melodic-moveit-setup-assistant
Track IK Plugin:

        sudo apt install ros-melodic-trac-ik-kinematics-plugin
ROS Controllers:

        sudo apt install ros-melodic-ros-controllers
FlexBE Behavior Engine:

        sudo apt install ros-melodic-flexbe-behavior-engine
FlexBE App:

        cd *omtp_workspace_name*/src
        git clone https://github.com/FlexBE/flexbe_app.git
Download generic FlexBE states:

        git clone https://github.com/FlexBE/generic_flexbe_states

### How to run code
Clone repo into your *ros_workspace/src* folder and run *catkin_make* in your workspace.
**Lecture 1:**

        roslaunch omtp_support visualize_omtp_factory.launch

**Lecture 5:**

        roslaunch omtp_lecture5 omtp_lecture5_environment.launch

and
        
        roslaunch omtp_lecture5 lecture5_assignment3.launch

**Lecture 6:**

        roslaunch omtp_lecture6 lecture6_assignment3.launch

and
        
        rosrun omtp_lecture6 lecture6_assignment4.py

**Lecture 7:**

        roslaunch omtp_gazebo omtp_pick_demo.launch

and
        
        rosservice call /start_spawn

and

        roslaunch flexbe_app flexbe_full.launch
---
# Project Description

This repository workflow is organised as follows: **master** branch has commits with complete lecture materials or documentation changes. Each complete lecture commit here is tagged as "lecX_submission". Furthermore, branch **develop** is used as a development (duuh) place where we, as a team, combine our work before merging into master branch.

### Lecture: 1. Building a Robot Simulation Environment in ROS

**Keywords** 

URDF, Xacro, RViz, ROS

**Tasks**

* URDF tutorials
* XACRO tutorials
* Remove 4 bins
* Move 1 bin
* Add a simple shape & change its parameters (color, origin, location, size)
* Add another UR5 & Add ABB robot

**Final commit**
[Lecture 1 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/6f7f11de4b88b6e27c2892934e1aa391cecee6e2)


**Notes**

To convert *file_name.xacro* to file_name.urdf:

        rosrun xacro xacro file_name.xacro > file_name.urdf

To check if file_name.urdf has no errors:

        check_urdf file_name.urdf

To open environment in RViz:
        
        roslaunch omtp_support visualize_omtp_factory.launch 

**Results (Image)**

![Factory Floor](images/lec1.png)

---
### Lecture: 3 & 4 Dynamic Movement Primitives. 

**Keywords** 

DMP, Dynamics, MATLAB, MuJoCo

**Tasks**

* Assignment 1: Implement and test DMPs
* Assignment 2: Domonstrate motion with DMPs ane execute in simulation
* Assignment 3: (Optional): Use DMPs to execute simple Pick and Place
  * *Status: Not done completely, can not make linear movements work 100%.*
* Assignment 4: (Optional): Detect contacts based on simulated forceres
  *  *Status: Not done.*

**Final commit**
[Lecture 3/4 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/8e2aef50f4148a89171e2e8748c246cbd05daa6e)

**Notes**

* Assignment 1 has issues on plotting the computed Quaternions, but can be solved by accessing the stored vector. 
* Third exercise is partually implemented but has issues of getting exact motions. Therefore, replaced back to the manual pick and place. 

**Results (Image & GIFs)**

* Computed trajectory (Red line) and reference (Blue stripes)

![tf view_frames](images/l4_E1.PNG)

* DMP movement 

![Pick and Place](images/L4_E2.1.gif)

* Pick and place

![Pick and Place](images/L4_E3.gif)


### Lecture: 5. Manipulation with MoveIt

**Keywords** 

Gazebo, MoveIt, MoveIt Commannder, MoveIt Setup Assistant

**Tasks**

* Assignment 1: Create a *MoveIt* package using *MoveIt Setup Assistant*
    - To open *MoveIt Setup Assistant*:

            roslaunch moveit_setup_assistant setup_assistant.launch 

* Assignment 2: Use MoveIt Commander to execute script of motions       
    - To open *MoveIt* environment:

            roslaunch omtp_moveit_config demo.launch

    - To open *MoveIt Commander*:

            rosrun moveit_commander moveit_commander_cmdline.py 

    - To load your script to *MoveIt Commander* (inside commander terminal):

            load <your_script_name>

* Assignment 3: Complete *lecture5_assignment3.py* code
    - To start factory simulation in *Gazebo* (IMPORTANT: Press "Start Simulation")

            roslaunch omtp_lecture5 omtp_lecture5_environment.launch

    - To launch script from *lecture5_assignment3.py*:

            roslaunch omtp_lecture5 lecture5_assignment3.launch


**Final commit**
[Lecture 5 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/05d81876708b4785073367633ae2f0eec7f42097)


**Notes**

* After creating MoveIt config package, edit *ros_controllers.yaml*, add name: **robot1**/robot1_controller and **robot2**/robot1_controller
* Python script *lecture5_assignment3.py* can be used both with RVIZ (see "Assignment 2" description) *demo.launch* and in *Gazebo* (see "Assignment 3").

**Results (GIF)**

![Factory Floor](images/lec5.gif)

### Lecture: 6. Object detection and grasping

**Keywords** 

Gazebo, MoveIt, Logical Camera, tf2, ROS Services

**Tasks**

* Assignment 1: Add two logical cameras, configure them and check.
    - To open Gazebo simulation world:

            roslaunch omtp_lecture6 lecture6_assignment1.launch 

* Assignment 2: Generate PDF with *tf* frames.       
    - Open simulation from assignment 1
    - Run:
  
            tf view_frames

* Assignment 3: Complete the script *lecture6_assignment3.py*.
    - To run completed script:

            roslaunch omtp_lecture6 lecture6_assignment3.py

* Assignment 4: Write *Python* script to pick and place the object into bin.
    - To open Gazebo simulation world:
            
            roslaunch omtp_lecture6 lecture6_assignment3.launch 

    - To run completed script:

            roslaunch omtp_lecture6 lecture6_assignment4.py

**Final commit**
[Lecture 6 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/3a78932ef9ed63dcdf3a6a8a9da0ba1a1cd42c74)


**Notes**

* Assignment 4 challenge (fill the bin with many objects) is not done. No time for it...
* Don't forget to press "start' inside the simulation before you run your script. Otherwise nothing will happen.

**Results (Image & GIF)**

* Frames tree

![tf view_frames](images/lec6_frames.png)
* Pick and place simulation

![Pick and Place](images/lec6.gif)
---
### Lecture: 7. Behavior Design with State Machines

**Keywords** 

FlexBE, Gazebo, State Machines

**Tasks**

* Assignment 1: Build the pick and place pipeline similar to the one on the next slide
      - To run the simulation
  
            roslaunch omtp_gazebo omtp_pick_demo.launch
            rosservice call /start_spawn
            roslaunch flexbe_app flexbe_full.launch

* Assignment 2: Use the conveyor state to bring an object to the area where the camera can detect it and where robot1 can pick it up  
      - Status: Not done.

**Final commit**
[Lecture 7 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/86582f7f9b94a8bc35ca17257fc5ab024e3f37ca)

**Notes**

* Assignment 2 has not been completed (no time)
* Don't forget to press "start' inside the simulation before you run your state engine. Otherwise nothing will happen.

**Results (Image & GIF)**

![Pick and Lift behavior](images/lec7_ex1.gif)
---
### Lecture: 8. CNNs in Practical Robotic Applications

**Keywords** 

CNN, Deep Learning, Google Colab, MobilNet

**Tasks**

* Assignment 1: To setup the program in Google Colab and to record a video, detecting mask, on each group member.
* Assignment 2 (Optional): Replace MobilNet CNN with some other CNN and show how it performs. *Status: Not done.*
* Assignment 3 (Optional): Train CNN to recognise objects of logical camera and use it in OMTP ROS environment. *Status: Not done.*


**Final commit**
[Lecture 8 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/e0198a87aa9599ae1bf5b09baf8cc9e7c982b22c)


**Notes**

* We followed the lecture in Google Colab. The program can also be run locally on a computer.
* If you run locally, worth to know that there is tensorflow-gpu package that lets you use GPU instead of CPU. See tutorial https://www.codingforentrepreneurs.com/blog/install-tensorflow-gpu-windows-cuda-cudnn/
* Training model in Google Colab takes about 10-15minutes, be patient.
* Using Google Colab, don't forget to set Notebook Settings to use GPU. Edit -> Notebook Settings.

**Results (Image & GIFs)**

* Some random image used with detect_mask_image.py

![tf view_frames](images/lec8_example.png)

* Eugene in detect_mask_video.py

![Pick and Place](images/lec8_eugene.gif)

* Hugo in detect_mask_video.py

![Pick and Place](images/l8_hugo.gif)

---
### Lecture: 9. Deep Reinforcement Learning for Robot Control 

**Keywords** 

Deep learning, Reinforcement learning, CNN, Python

**Tasks**

* Assignment 1-4: Make a viritual env, install OpenAI gym, Tensorflow/board <=1.15.0 and Stable-Baselines.
* Assignment 5: Train the agent (DQN).
* Assignment 6: Observe the training with TensorBoard.

**Final commit**
[Lecture 9 submission](https://bitbucket.org/group_862/definitely-not-an-omtp/commits/61cf86a6d9c01940804f85bcbd7a497c25cea8c0)

**Notes**

* Made both DQN and OOP. The current version in the repo is for PPO, but only few changes needed to convert to DQN

**Results (Image & GIFs)**

* PPO with 25.000 iterations

![PPO 25000](images/l9_PPO1_25000.gif)

* PPO with 300.000 iterations

![PPO 300000](images/l9_PPO1_300000.gif)

* TensorBoard with PPO 25.000 iterations

![TensorBoard PPO_25000](images/l9_OOP1_25k.png)

* TensorBoard with PPO 300.000 iterations

![TensorBoard PPO_300000](images/l9_OOP1_300k.png)
