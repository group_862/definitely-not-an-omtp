#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from flexbe_manipulation_states.moveit_to_joints_dyn_state import MoveitToJointsDynState as flexbe_manipulation_states__MoveitToJointsDynState
from omtp_factory_flexbe_states.detect_part_camera_state import DetectPartCameraState
from omtp_factory_flexbe_states.compute_grasp_state import ComputeGraspState
from omtp_factory_flexbe_states.vacuum_gripper_control_state import VacuumGripperControlState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu Jun 11 2020
@author: Group 862
'''
class Robot1PickandPlaceSM(Behavior):
	'''
	Behavior for pick-n-place, using conveyor, logical camera, and robot 1.
	'''


	def __init__(self):
		super(Robot1PickandPlaceSM, self).__init__()
		self.name = 'Robot 1 Pick and Place'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		r1_home = [1.57, -1.57, 0.43, -1.57, -1.57, 0.0]
		r1_pregrasp = [1.57, -0.98, 0.0, -2.17, -1.57, 0.0]
		r1_joint_names = ['robot1_elbow_joint', 'robot1_shoulder_lift_joint', 'robot1_shoulder_pan_joint', 'robot1_wrist_1_joint', 'robot1_wrist_2_joint', 'robot1_wrist_3_joint']
		r1_move_group = 'robot1'
		r1_tool_link = 'vacuum_gripper1_suction_cup'
		r1_gripper_service = '/gripper1/control'
		# x:8 y:127, x:761 y:310
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.r1_home = r1_home
		_state_machine.userdata.r1_computed_pregrasp = []
		_state_machine.userdata.r1_joint_names = r1_joint_names
		_state_machine.userdata.object_pose = []
		_state_machine.userdata.r1_pregrasp = r1_pregrasp
		_state_machine.userdata.r1_computed_pregrasp2 = []

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:505 y:59
			OperatableStateMachine.add('Move robot1 to Home',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_move_group, action_topic='/move_group'),
										transitions={'reached': 'Detect Object', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_home', 'joint_names': 'r1_joint_names'})

			# x:840 y:49
			OperatableStateMachine.add('Detect Object',
										DetectPartCameraState(ref_frame='world', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'Compute PreGrasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'object_pose'})

			# x:1002 y:334
			OperatableStateMachine.add('Move robot1 to PreGrasp',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_move_group, action_topic='/move_group'),
										transitions={'reached': 'Compute Grasp', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_computed_pregrasp', 'joint_names': 'r1_joint_names'})

			# x:908 y:481
			OperatableStateMachine.add('Compute Grasp',
										ComputeGraspState(group=r1_move_group, offset=0.16, joint_names=r1_joint_names, tool_link=r1_tool_link, rotation=3.14),
										transitions={'continue': 'Move robot1 to Grasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'object_pose', 'joint_values': 'r1_computed_grasp', 'joint_names': 'r1_joint_names'})

			# x:665 y:556
			OperatableStateMachine.add('Move robot1 to Grasp',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_move_group, action_topic='/move_group'),
										transitions={'reached': 'Enable Gripper', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_computed_grasp', 'joint_names': 'r1_joint_names'})

			# x:431 y:507
			OperatableStateMachine.add('Enable Gripper',
										VacuumGripperControlState(enable='true', service_name=r1_gripper_service),
										transitions={'continue': 'Compute PreGrasp_2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:327 y:118
			OperatableStateMachine.add('Move robot1 to Home_2',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_move_group, action_topic='/move_group'),
										transitions={'reached': 'finished', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_home', 'joint_names': 'r1_joint_names'})

			# x:991 y:173
			OperatableStateMachine.add('Compute PreGrasp',
										ComputeGraspState(group=r1_move_group, offset=0.2, joint_names=r1_joint_names, tool_link=r1_tool_link, rotation=3.14),
										transitions={'continue': 'Move robot1 to PreGrasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'object_pose', 'joint_values': 'r1_computed_pregrasp', 'joint_names': 'r1_joint_names'})

			# x:300 y:387
			OperatableStateMachine.add('Compute PreGrasp_2',
										ComputeGraspState(group=r1_move_group, offset=0.4, joint_names=r1_joint_names, tool_link=r1_tool_link, rotation=3.15),
										transitions={'continue': 'Move robot1 to PreGrasp_2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'object_pose', 'joint_values': 'r1_computed_pregrasp2', 'joint_names': 'r1_joint_names'})

			# x:302 y:244
			OperatableStateMachine.add('Move robot1 to PreGrasp_2',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=r1_move_group, action_topic='/move_group'),
										transitions={'reached': 'Move robot1 to Home_2', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'r1_computed_pregrasp2', 'joint_names': 'r1_joint_names'})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
