import gym
import time
 
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import PPO1
 
env = gym.make('CartPole-v1')
model = PPO1(MlpPolicy, env, verbose=1, tensorboard_log='./logs')

train_model = True
total_training_times = 300000
model_filename = 'PPO1_cartpole_' + str(total_training_times)
time_start = time.time()
if train_model:
    model.learn(total_timesteps=total_training_times)
    model.save(model_filename)
 
else:
    model = PPO1.load(model_filename)
    obs = env.reset()
    while True:
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.step(action)
        env.render()
        if dones is True:
            env.close()
            break
        
time_stop = time.time()
print('Trained {0} attempts in {1:1.2f} seconds'.format(total_training_times, time_stop - time_start))